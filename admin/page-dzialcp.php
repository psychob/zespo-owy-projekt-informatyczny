<?php
 // sprawdzamy prawa użytkownika
 if (!($user->getRights()->czyJestemRedaktoremDzialu(null)))
 {
  header('location: index.php?what=privilage');
  die;
 }
 
 $theme->assign('page', 'dzialcp');
 
 $sub = isset($_GET['sub']) && !empty($_GET['sub']) ? $_GET['sub'] : 'list';
 
 switch ( $sub )
 {
  case 'list-users':
    // pobieramy redaktorów z danego działu
    $id = intval($_GET['did']);
   
   
    $red = "select * from Dzial inner join UzytkownikRedaktor on ( Dzial.idDzialu = UzytkownikRedaktor.idDzialu ) ".
           "inner join Uzytkownik on (UzytkownikRedaktor.idUzytkownika = Uzytkownik.idUzytkownika) where Dzial.idDzialu = ".$id;
    
    $red = $db->query($red)->fetchAll();
    
    $theme->assign('tab', $red);
    
    $theme->assign('sub', 'list-user');
    $theme->assign('did', $id);
   break;
  
  case 'add-user':
   $theme->assign('sub', 'add-user');
   
   $sql = "select * from Uzytkownik order by `nazwaWyswietlana`;";
   $sql = $db->query($sql);
   
   $theme->assign('tab', $sql->fetchAll());
    $id = intval($_GET['did']);
   $theme->assign('did', $id);
   break;
  
  case 'add-user-submit':
   $id = intval($_POST['user-id']);
   $did = intval($_POST['did']);
   $sql = "insert into UzytkownikRedaktor values ( null, {$id}, {$did} );";
   $sql = $db->query($sql);
   
   header('location: index.php?what=dzialcp&sub=list-users&did='.$did);
   die;
   
  case 'revoke':
   $id = intval($_GET['uid']);
   $did = intval($_GET['did']);
   
   $sql = "delete from UzytkownikRedaktor where `idUzytkownika` = {$id} and `idDzialu` = {$did};";
   $sql = $db->query($sql);
   
   header('location: index.php?what=dzialcp&sub=list-users&did='.$did);
   die;
  
  case 'list':
  default:
   $theme->assign('sub', 'list');
   
   // wyświetlamy zainteresowane działy
   $sql = "select Dzial.idDzialu as id, Dzial.nazwa as name from Dzial inner join UzytkownikRedaktorDzialu on ( Dzial.idDzialu ".
          " = UzytkownikRedaktorDzialu.idDzialu ) where UzytkownikRedaktorDzialu.idUzytkownika = {$user->getID()};";
   $sql = $db->query($sql);
   
   $theme->assign('tab', $sql->fetchAll());
 }