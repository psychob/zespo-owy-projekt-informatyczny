<?php
 // sprawdzamy prawa użytkownika
 if (!($user->getRights()->czyJestemDyrektorem() || $user->getRights()->czyJestemRedaktoremNaczelnym()))
 {
  header('location: index.php?what=privilage');
  die;
 }
 
 $sub = isset($_GET['sub']) && !empty($_GET['sub']) ? $_GET['sub'] : 'list';
 
 $theme->assign('page', 'user');
 
 switch ( $sub )
 {
  case 'list':
   // wyświetlamy...
   $theme->assign('sub', 'list');
   
   // pobieramy działy...
   $sql = "select * from `Uzytkownik` order by `idUzytkownika` asc;";
   $sql = $db->query($sql);
   
   $max_page = $sql->count();
   
   if ( isset($_GET['page']) && !empty($_GET['page']))
    $page = intval($_GET['page']);
   else
    $page = 0;
   
   $sql = "select * from `Uzytkownik` order by `idUzytkownika` asc limit ".($page*30).", 30;";
   $sql = $db->query($sql);
   
   $theme->assign('tab', $sql->fetchAll());
   $theme->assign('max_pages', (int)($max_page/30));
   $theme->assign('current_page', $page);
   
   if ( isset($_GET['status']) && !empty($_GET['status']) )
    $theme->assign('status', $_GET['status']);
   break;
   
  case 'add':
   $theme->assign('sub', 'add');
   break;
  
  case 'add-submit':
   
   if ( ( isset($_POST['login']) && !empty($_POST['login']) ) &&
        ( isset($_POST['haslo1']) && !empty($_POST['haslo1']) ) &&
        ( isset($_POST['rights']) && !empty($_POST['rights']) ) &&
        ( isset($_POST['email']) && !empty($_POST['email']) ) )
   {
    // wszystkie dane są
  
    // sprawdzamy czy poprawne
    if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false)
    {
     header('location: index.php?what=user&error=email');
     die;
    }

    // sprawdzamy czy jest już taki zainteresowany tutaj
    $login = $db->escapeString($_POST['email']);
    
    $sql = $db->query("select idUzytkownika from `Uzytkownik` where `email` = '".$login."';");
    if ( $sql->count() )
    {
     header('location: index.php?what=user&error=already');
     die;
    }
    
    $rights = intval($_POST['rights']);
    $rights = $rights < 1 || $rights > 3 ? 1 : $rights;
    
    $sql = "insert into `Uzytkownik` (nazwaWyswietlana, email, haslo, idPrawa) ".
           "values ( '".$db->escapeString($_POST['login'])."', '".$login."', '".md5($_POST['haslo1'])."', ".$rights.");";
    
    $sql = $db->query($sql);
   
   header('location: index.php?what=user&status=ok');
   die;
  } else
  {
   header('location: index.php?what=user&error=empty');
   die;
  }
  
   die;
   
  case 'edit':
   $theme->assign('sub', 'edit');
   $id = intval($_GET['id']);
   
   $sql = $db->query( "select * from `Uzytkownik` where `idUzytkownika` = ".$id." limit 1");
   $theme->assign('item', $sql->current());
   break;
   
  case 'edit-submit':
   if ( ( isset($_POST['login']) && !empty($_POST['login']) ) &&
        ( isset($_POST['haslo1']) ) &&
        ( isset($_POST['rights']) && !empty($_POST['rights']) ) &&
        ( isset($_POST['email']) && !empty($_POST['email']) ) )
   {
    if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false)
    {
     header('location: index.php?what=user&error=email'); // TO DO
     die;
    }
    
    $id = intval($_POST['id']);

    $rights = intval($_POST['rights']);
    $rights = $rights < 1 || $rights > 3 ? 1 : $rights;
    
    if ( empty($_POST['haslo1']) )
    {
     $sql= "update `Uzytkownik` set `nazwaWyswietlana` = '".$db->escapeString($_POST['login'])."', ".
                                   "`email` = '".$db->escapeString($_POST['email'])."', ".
                                   "`idPrawa` = ".$rights." where `idUzytkownika` = {$id} limit 1;";
    } else
    {
     $sql= "update `Uzytkownik` set `nazwaWyswietlana` = '".$db->escapeString($_POST['login'])."', ".
                                   "`email` = '".$db->escapeString($_POST['email'])."', ".
                                   "`haslo` = '".md5($_POST['email'])."', ".
                                   "`idPrawa` = ".$rights." where `idUzytkownika` = {$id} limit 1;";
    }
    
    $sql = $db->query($sql);
    
    header('location: index.php?what=user&status=user-mod');
    //die;
   } else
   {
    /// TODO
    header('location: index.php?what=user&error=empty');
    die;
   }
   die;
   
  case 'delete':
   $id = intval($_GET['id']);
   
   $sql = "delete from `Uzytkownik` where `idUzytkownika` = ".$id." limit 1;";
   $sql = $db->query($sql);
   
   header('location: index.php?what=user&status=deleted-user');
   die;
   
  case 'edit-priv':
   $theme->assign('sub', 'mod-priv');
   
   $id = intval($_GET['id']);
   
   $sql = "select nazwaWyswietlana from Uzytkownik where idUzytkownika = {$id};";
   $sql = $db->query($sql);
   
   $theme->assign('user_name', $sql['nazwaWyswietlana']);
   $theme->assign('user_id', $id);
   
   $sql = "select `Dzial`.`idDzialu` as id, `Dzial`.`nazwa` as name from `UzytkownikRedaktor` ".
          "inner join `Dzial` on ( `UzytkownikRedaktor`.`idDzialu` = `Dzial`.`idDzialu` ) where ".
          "idUzytkownika = ".$id;
   
   $theme->assign('red_rights', $db->query($sql)->fetchAll());
   
   $sql = "select `Dzial`.`idDzialu` as id, `Dzial`.`nazwa` as name from `UzytkownikRedaktorDzialu` ".
          "inner join `Dzial` on ( `UzytkownikRedaktorDzialu`.`idDzialu` = `Dzial`.`idDzialu` ) where ".
          "idUzytkownika = ".$id;
   
   $theme->assign('redd_rights', $db->query($sql)->fetchAll());
   break;
  
  case 'revoke-priv-r':
   $uid = intval($_GET['uid']);
   $did = intval($_GET['did']);
   
   $sql = "delete from `UzytkownikRedaktor` where `idDzialu` = ".$did." and `idUzytkownika` = ".$uid." limit 1;";
   $db->query($sql);
   
   header('location: index.php?what=user&sub=edit-priv&id='.$uid);
   die;
  
  case 'revoke-priv-rd':
   $uid = intval($_GET['uid']);
   $did = intval($_GET['did']);
   
   $sql = "delete from `UzytkownikRedaktorDzialu` where `idDzialu` = ".$did." and `idUzytkownika` = ".$uid." limit 1;";
   $db->query($sql);
   
   header('location: index.php?what=user&sub=edit-priv&id='.$uid);
   die;
 }
 