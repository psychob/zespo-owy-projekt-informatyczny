<?php

$theme->assign('descend', 'asc');

$theme->assign('page', 'manage');
$action = isset($_GET['action']) && !empty($_GET['action']) ? $_GET['action'] : 'print';
$sub = empty($_GET['sub']) ? '' : $_GET['sub'];


if ($action == 'print') {
    if (!empty($_GET['orderby'])) {
        $descend = $_GET['descend'] == 'desc' ? 'asc' : 'desc';
        $orderby = $_GET['orderby'];
        if ($descend == 'asc') {
            $theme->assign('descend', 'asc');
        } else {
            $theme->assign('descend', 'desc');
        }
    } else {
        $orderby = empty($_GET['orderby']) ? 'id' : $_GET['orderby'];
        $descend = empty($_GET['descend']) ? 'asc' : $_GET['descend'];
    }


    if ( $user->getRights()->czyJestemDyrektorem() )
    {
     $sql = "select p.id,p.tytul,u.nazwaWyswietlana,p.dataPublikacji, p.akceptacja,"
            . " d.nazwa from Publikacja p, Uzytkownik u,Dzial d "
            . "where u.idUzytkownika = p.idAutora and d.idDzialu = p.idDzialu"
            . " order by $orderby $descend;";
    } else
    {
     // redaktor naczelny
     $sql = "select p.id,p.tytul,u.nazwaWyswietlana,p.dataPublikacji, p.wstepnaAkceptacja as 'akceptacja',"
            . " d.nazwa from Publikacja p, Uzytkownik u,Dzial d "
            . "where u.idUzytkownika = p.idAutora and d.idDzialu = p.idDzialu and ( 0 = 1 ";
     
     foreach ( $user->getRights()->redaktor2 as $v )
      $sql .= " or ( p.idDzialu = {$v} )";
      
     $sql .= ") order by $orderby $descend;";
    }
    
    
    $sql = $db->query($sql);
    $theme->assign('tab', $sql->fetchAll());
} else if ($action == 'manage') {
    switch ($sub) {
        case 'accept':
            $id = $_GET['id'];
         if ( $user->getRights()->czyJestemDyrektorem() )
            $sql = "update Publikacja set akceptacja=1 where id='$id';";
         else
            $sql = "update Publikacja set wstepnaAkceptacja=1 where id='$id';";
            $sql = $db->query($sql);
            header('location: index.php?what=manage');
            die;
            break;

        case 'cancelAccept':
            $id = $_GET['id'];
         if ( $user->getRights()->czyJestemDyrektorem() )
            $sql = "update Publikacja set akceptacja=0 where id='$id';";
         else
            $sql = "update Publikacja set wstepnaAkceptacja=0 where id='$id';";
            $sql = $db->query($sql);
            header('location: index.php?what=manage');
            die;
            break;
    }
}