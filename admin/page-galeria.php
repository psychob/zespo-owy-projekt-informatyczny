<?php
 // sprawdzamy prawa użytkownika
if (!($user->getRights()->czyJestemDyrektorem() ||
      $user->getRights()->czyJestemRedaktoremNaczelnym() ||
      $user->getRights()->czyJestemRedaktorem(null) ||
      $user->getRights()->czyJestemRedaktoremDzialu(null)))
 {
  header('location: index.php?what=privilage');
  die;
 }
 
 $sub = isset($_GET['sub']) && !empty($_GET['sub']) ? $_GET['sub'] : 'list';
 
 $theme->assign('page', 'galeria');
 
  switch ( $sub )
 {
  case 'list':
    // wyświetlamy...
    $theme->assign('sub', 'list');
    $osql = "";
    
    // jeśli jesteśmy szychą
    if ( $user->getRights()->czyJestemDyrektorem() )
     $osql = "select `idGaleria`,`nazwa`,`idUzytkownika`,`nazwaWyswietlana` from `Galeria` inner join `Uzytkownik` on (`idAutora` = `idUzytkownika`) order by `id` asc ";
    else 
    {
     $osql = "select `idGaleria`,`nazwa`,`idUzytkownika`,`nazwaWyswietlana` from `Galeria` inner join `Uzytkownik` on (`idAutora` = `idUzytkownika`) where ";
     if ( $user->getRights()->czyJestemRedaktorem(null) )
     {
      $osql .= " ( 0 = 1 ) ";
     
      foreach ( $user->getRights()->redaktor1 as $v )
      {
       $osql .= " or (`idAutora` = {$user->getID()} and `idDzialu` = {$v}) ";
      }
     }
     
     if ( $user->getRights()->czyJestemRedaktoremDzialu(null) )
     {
      if ( empty($user->getRights()->redaktor1) )
       $osql .= " (0 = 1 ) ";
      
      foreach ( $user->getRights()->redaktor2 as $v )
      {
       $osql .= " or (`idDzialu` = {$v}) ";
      }
     }
     
     $osql .= " order by `id` asc ";
    }

    // pobieramy działy...
   $sql = $osql;
   $sql = $db->query($sql);
   
   $max_page = $sql->count();
   
   if ( isset($_GET['page']) && !empty($_GET['page']))
    $page = intval($_GET['page']);
   else
    $page = 0;
   
   $sql = $osql." limit ".($page*30).", 30;";
   $sql = $db->query($sql);
   
   $theme->assign('tab', $sql->fetchAll());
   $theme->assign('max_pages', (int)($max_page/30));
   $theme->assign('current_page', $page);
   
   if ( isset($_GET['status']) && !empty($_GET['status']) )
   $theme->assign('status', $_GET['status']);     
      
         break;
   
  case 'add':
      $theme->assign('sub', 'add');
      
  // pokazujemy działy do których mamy prawa
  $sql = "select `idDzialu`, `nazwa` from Dzial where (0 = 1) ";
  
  foreach ( $user->getRights()->redaktor1 as $v )
   $sql .= " or (idDzialu = {$v}) ";
   
  foreach ( $user->getRights()->redaktor2 as $v )
   $sql .= " or (idDzialu = {$v}) ";
   
  $sql .= " order by `idDzialu` asc;";
      $sql = $db->query($sql);
   
      $theme->assign('tab', $sql);
      
      $sql = "select `idGaleria`, `nazwa` from Galeria order by `idGaleria` asc;";
      $sql = $db->query($sql);
   
      $theme->assign('tab2', $sql);
      break;
  
  case 'add-submit':
    if ( !isset($_POST['tytul']) || empty($_POST['tytul']) || !isset($_POST['tresc']) || empty($_POST['tresc']) )
    {
     header('location: index.php?what=galeria&sub=add&error=empty');
     die;
    }

    $nazwa = $db->escapeString($_POST['nazwa']);
    $opis = $db->escapeString($_POST['opis']);
    $idGal = intval($_POST['galeria']);
    $idDzi = intval($_POST['dzial']);
    $idAut = intval($_SESSION['user_id']);
    $akceptacja        = 0;
    $wstepnaAkceptacja = 0;
    
     
    $galeria->dodajGalerie($idGaleria, $nazwa, $opis, $idDzialu, $idAutora);

    header('location: index.php?what=galeria&sub=list&status=add');
          
      break;
   
  case 'edit':
      
   $theme->assign('sub', 'edit');
      
  $sql = "select `idDzialu`, `nazwa` from Dzial where (0 = 1) ";
  
  foreach ( $user->getRights()->redaktor1 as $v )
   $sql .= " or (idDzialu = {$v}) ";
   
  foreach ( $user->getRights()->redaktor2 as $v )
   $sql .= " or (idDzialu = {$v}) ";
   
  $sql .= " order by `idDzialu` asc;";
   $sql = $db->query($sql);
   
   $theme->assign('tab', $sql->fetchAll());
   
   $sql = "select `idGaleria`, `nazwa` from Galeria order by `idGaleria` asc;";
   $sql = $db->query($sql);
   
   $theme->assign('item', $sql->current());
      break;  
  
  case 'edit-submit':
      if ( !isset($_POST['nazwa']) || empty($_POST['nazwa']) ||
        !isset($_POST['opis']) || empty($_POST['opis']))
   {
    header('location: index.php?what=galeria&sub=edit&error=empty&id='.$id);
    die;
   }
   
   $nazwa = $db->escapeString($_POST['nazwa']);
   $opis = $db->escapeString($_POST['opis']);
   $idDzial = intval($_POST['dzial']);
   $id = intval($_POST['idGaleria']);

   $galeria->edytujGalerie($idGaleria, $nazwa, $opis, $idDzial, $idAutora);
   
   header('location: index.php?what=galeria&sub=list&status=edit');
   
   die;
      break;
   
  case 'delete':
  if (!isset($_GET['id']) && !empty($_GET['id']))
   {
    header('location: index.php?what=galeria&sub=list&error=empty');
    die;
   }
   $id = $_GET['id'];
   $galeria->usunGalerie($idGaleria);
   
   header('location: index.php?what=galeria&sub=list&status=delete');
   die;
      
      break;
 }

