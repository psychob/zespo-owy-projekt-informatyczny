<?php
 // sprawdzamy prawa użytkownika
 if (!($user->getRights()->czyJestemDyrektorem() || $user->getRights()->czyJestemRedaktoremNaczelnym()))
 {
  header('location: index.php?what=privilage');
  die;
 }
 
 $sub = isset($_GET['sub']) && !empty($_GET['sub']) ? $_GET['sub'] : 'list';
 
 $theme->assign('page', 'dzial');
 
 switch ( $sub )
 {
  case 'list':
   // wyświetlamy...
   $theme->assign('sub', 'list');
   
   // pobieramy działy...
   $sql = "select `idDzialu`,`nazwa`,`idUzytkownika`,`nazwaWyswietlana` from `Dzial` inner join `Uzytkownik` on (`idRedaktora` = `idUzytkownika`) order by `idDzialu` asc;";
   $sql = $db->query($sql);
   
   $max_page = $sql->count();
   
   if ( isset($_GET['page']) && !empty($_GET['page']))
    $page = intval($_GET['page']);
   else
    $page = 0;
   
   $sql = "select `idDzialu`,`nazwa`,`idUzytkownika`,`nazwaWyswietlana` from `Dzial` inner join `Uzytkownik` on (`idRedaktora` = `idUzytkownika`) order by `idDzialu` asc limit ".($page*30).", 30;";
   $sql = $db->query($sql);
   
   $theme->assign('tab', $sql->fetchAll());
   $theme->assign('max_pages', (int)($max_page/30));
   $theme->assign('current_page', $page);
   
   if ( isset($_GET['status']) && !empty($_GET['status']) )
    $theme->assign('status', $_GET['status']);
   break;
   
  case 'add':
   $theme->assign('sub', 'add');
   
   $sql = "select idUzytkownika, nazwaWyswietlana from Uzytkownik order by nazwaWyswietlana asc;";
   $sql = $db->query($sql);
   
   $theme->assign('tab', $sql);
   break;
  
  case 'add-submit':
   if ( !isset($_POST['nazwa']) || empty($_POST['nazwa']) || !isset($_POST['redaktor']) || empty($_POST['redaktor']) )
   {
    header('location: index.php?what=dzialy&sub=add&error=empty');
    die;
   }
   
   $nazwa = $db->escapeString($_POST['nazwa']);
   $idRed = intval($_POST['redaktor']);
   
   //$sql = "insert into `Dzial` (`nazwa`, `idRedaktora`) VALUES ('$nazwa', '$idRed');";
   //$sql = $db->query($sql);
   
   
   //insert do tabeli
   $sql = "select `idDzialu` from `Dzial` Where nazwa = '$nazwa';";
   $idDzialu = $db->query($sql);
     
   $sql = "insert into `UzytkownikRedaktorDzialu` (`idUzytkownika`, `idDzialu`) VALUES ('$idRed', '".$idDzialu['idDzialu']."');";
   $sql = $db->query($sql);

   header('location: index.php?what=dzialy&sub=list&status=add');
   
   die;
   break;
   
  case 'edit':
   $theme->assign('sub', 'edit');
   
   $sql = "select idUzytkownika, nazwaWyswietlana from Uzytkownik order by nazwaWyswietlana asc;";
   $sql = $db->query($sql);
   
   $theme->assign('tab', $sql->fetchAll());
   
   $sql = "select * from Dzial where idDzialu = ".intval($_GET['id']).";";
   $sql = $db->query($sql);
   $theme->assign('item', $sql->current());
   break;
  
  case 'edit-submit':
   if ( !isset($_POST['nazwa']) || empty($_POST['nazwa']) ||
        !isset($_POST['redaktor']) || empty($_POST['redaktor']) ||
        !isset($_POST['id']) || empty($_POST['id']) )
   {
    header('location: index.php?what=dzialy&sub=edit&error=empty&id='.$id);
    die;
   }
   
   $nazwa = $db->escapeString($_POST['nazwa']);
   $idRed = intval($_POST['redaktor']);
   $id    = intval($_POST['id']);
   
   $sql = "update Dzial set `nazwa` = '".$nazwa."', `idRedaktora` = '".$idRed."' where idDzialu = '".$id."' limit 1;";
   $db->query($sql);
   //update tabeli
   $sql = "select `id` From `UzytkownikRedaktorDzialu` where idDzialu = '".$id."' limit 1;";
   $idURD = $db->query($sql);
   $sql = "update `UzytkownikRedaktorDzialu` Set `idUzytkownika` = '".$idRed."', `idDzialu` = '".$id."' where id = '".$idURD['id']."' limit 1;";
   echo $sql;
   $db->query($sql);   
   
   header('location: index.php?what=dzialy&sub=list&status=edit');
   
   die;
   break;
   
  case 'delete':
   if (!isset($_GET['id']) && !empty($_GET['id']))
   {
    header('location: index.php?what=dzialy&sub=list&error=empty');
    die;
   }
   
   $sql = "delete from Dzial where idDzialu = ".intval($_GET['id'])." limit 1";
   $db->query($sql);
   
   header('location: index.php?what=dzialy&sub=list&status=delete');
   die;
   break;
 }
 