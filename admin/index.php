<?php
 require_once '../lib/smarty/Smarty.class.php';
 require_once '../lib/MySQL.php';
 
 require_once '../includes/Uzytkownik.php';
 require_once '../includes/publikacja.php';
 
 session_start();
 
 $db = new MySQL( "sql341389", "tI2!dP3%", "sql341389", "sql3.freesqldatabase.com");
 $theme = new Smarty();
 $user = new Uzykownik($db);
 $article = new Publikacja($db);
 
 $user->isLogged();
 
 $theme->setCompileDir('../tpl/cmp/admin');
 $theme->setTemplateDir('../tpl/tpl/admin');
 $theme->setConfigDir('../tpl/cfg/admin');
 
 $theme->debugging = true;
 
 // prawa
 $theme->assign('prawa_dzial', $user->getRights()->czyJestemDyrektorem() || $user->getRights()->czyJestemRedaktoremNaczelnym());
 $theme->assign('prawa_user', $user->getRights()->czyJestemDyrektorem() || $user->getRights()->czyJestemRedaktoremNaczelnym());
 $theme->assign('prawa_publikacja', $user->getRights()->czyJestemDyrektorem() ||
                                    $user->getRights()->czyJestemRedaktoremNaczelnym() ||
                                    $user->getRights()->czyJestemRedaktorem(null) ||
                                    $user->getRights()->czyJestemRedaktoremDzialu(null));
 $theme->assign('prawa_akceptacja', $user->getRights()->czyJestemDyrektorem() ||
                                    $user->getRights()->czyJestemRedaktoremDzialu(null));
 $theme->assign('prawa_dzialcp', $user->getRights()->czyJestemRedaktoremDzialu(null));
 
 $what = isset($_GET['what']) && !empty($_GET['what']) ? $_GET['what'] : 'index';
 
 switch ( $what )
 {
  case 'dzialy':
   require_once './page-dzial.php';
   break;
  
  case 'user':
   require_once './page-user.php';
   break;
  
  case 'publikacja':
   require_once './page-publikacja.php';
   break;
  
  case 'manage':
   require_once './page-manage.php';
   break;
  
  case 'dzialcp':
   require_once './page-dzialcp.php';
   break;

  case 'galeria':
   require_once './page-galeria.php';
   break;
  
  case 'privilage':
   $theme->assign('page', 'privilage');
   break;
   
  case 'index':
  default:
   $theme->assign('page', 'index-page');
 }
 
 $theme->display('index.tpl');
