<?php

 class MySQL
 {
  public function getDriverName()
  {
   return __CLASS__;
  }
  
  public function getDatabaseName()
  {
   return "MySQL";
  }
  
  private $dbLink  = null;
  private $dbCount = 0;
  private $dbDebug = [];
  private $dbTime  = 0.0;

  public function __construct( $login, $pass, $db, $host, $port = 3306 )
  {
   $this->dbLink = mysqli_connect($host, $login, $pass, $db, $port);

   $this->query("set names utf8;");
  }

  public function __destruct( )
  {
   mysqli_close($this->dbLink);
  }

  // --------------------------------------------------------------------------

  public function getQueryCount( )
  {
   return $this->dbCount;
  }

  public function getAffectedRows( )
  {
   return mysqli_affected_rows($this->dbLink);
  }

  public function getQueryTime()
  {
   return $this->dbTime;
  }

  public function escapeString($str)
  {
   return mysqli_real_escape_string($this->dbLink, $str);
  }

  // --------------------------------------------------------------------------

  private function _appendQuery($query, &$res, $time )
  {
   $this->dbDebug[] = [ 'query'     => $query,
                        'error_no'  => mysqli_errno($this->dbLink),
                        'error_str' => mysqli_error($this->dbLink),
                        'affected'  => $this->getAffectedRows(),
                        'time'      => $time ];
   $this->dbTime += $time;
   $this->dbCount++;
  }

  public function query( $str )
  {
   $mtime = microtime(true);
   $query = mysqli_query($this->dbLink, $str);
   $this->_appendQuery($str, $query, microtime(true) - $mtime);

   return new MySQL_Query( $query, $str, $this->getAffectedRows() );
  }
 }
 
 class MySQL_Query implements ArrayAccess, Iterator, Countable
 {
  private $qlnk   = null;
  private $qcount = 0;
  private $currentRow = [];
  private $currentKey = 0;

  public function __construct( $qlnk, $str, $count )
  {
   $this->qlnk = $qlnk;
   $this->qcount = $count;

   if ( !is_bool($this->qlnk) )
    $this->currentRow = mysqli_fetch_array ($this->qlnk, MYSQLI_ASSOC);
  }

  public function __destruct()
  {
   if (!is_bool($this->qlnk))
    mysqli_free_result ($this->qlnk);
  }

  public function count()
  {
   return $this->qcount;
  }
  
  public function getBool( )
  {
   return $this->qlnk;
  }

  public function offsetExists($offset)
  {
   return isset($this->currentRow[$offset]);
  }

  public function offsetGet($offset)
  {
   return $this->currentRow[$offset];
  }

  public function offsetSet($offset, $value)
  {
   // nic z tym nie robimy nasz obiekt jest stały;
  }

  public function offsetUnset($offset)
  {
   // nic nie robimy
  }

  public function current()
  {
   return $this->currentRow;
  }

  public function key()
  {
   return $this->currentKey;
  }

  public function next()
  {
   $this->currentRow = mysqli_fetch_array($this->qlnk, MYSQLI_ASSOC);
   $this->currentKey++;
  }

  public function rewind()
  {
   $this->currentKey = -1;
   mysqli_data_seek($this->qlnk, 0);
   $this->next();
  }

  public function valid()
  {
   return $this->currentKey < $this->qcount;
  }

  public function fetchAll( )
  {
   $ret = [];
   for ( $this->rewind(); $this->valid(); $this->next() )
    $ret[] = $this->current();
   return $ret;
  }
 };