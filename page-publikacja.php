<?php
$theme->assign('page', 'publikacja');

 require_once './includes/publikacja.php';
  require_once './includes/komentarz.php';
 
 $pub = new Publikacja($db);
 $pub = $pub->pobierzPublikacje($_GET['id']);
 
 $koment = new komentarz();
 
 $theme->assign('pub', $pub);
 $theme->assign('user_logged', $user->isLogged());
 
 
 $idPubl = $_GET['id'];
$theme->assign('idPub', $idPubl);

$theme->assign('user_info', $user->getUserInfo());

if(isset($_POST['tresc']) && !empty($_POST['tresc']))
{
    if(isset($_POST['idAut']))
    {
        $idPub = $_POST['idPub'];
        $idAutor = $_POST['idAut'];
        $tresc = $db->escapeString($_POST['tresc']);
        $koment->DodajKomentarz($idPub, $idAutor, $tresc);
    }
    else
    {
        $idPub = $_POST['idPub'];
        $tresc = $db->escapeString($_POST['tresc']);
        $nazwaAutora = $db->escapeString($_POST['autor']);
        $koment->DodajKomentarzJakoGosc($idPub, $nazwaAutora, $tresc);
    }
}

      $sql = "select * from Komentarz WHERE idPublikacji='$idPubl';";
      $sql = $db->query($sql);
   
      $theme->assign('kom', $sql);