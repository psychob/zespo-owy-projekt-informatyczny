-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 24, 2014 at 11:20 AM
-- Server version: 5.5.37-0ubuntu0.13.10.1
-- PHP Version: 5.5.3-1ubuntu2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `szkolnycms`
--

-- --------------------------------------------------------

--
-- Table structure for table `Dzial`
--

CREATE TABLE IF NOT EXISTS `Dzial` (
  `idDzialu` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(30) NOT NULL,
  `idRedaktora` int(11) NOT NULL,
  PRIMARY KEY (`idDzialu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Galeria`
--

CREATE TABLE IF NOT EXISTS `Galeria` (
  `idGaleria` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(30) NOT NULL,
  `opis` varchar(30) DEFAULT NULL,
  `idDzialu` int(11) NOT NULL,
  `idAutora` int(11) NOT NULL,
  PRIMARY KEY (`idGaleria`),
  KEY `idAutora` (`idAutora`),
  KEY `idDzialu` (`idDzialu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Komentarz`
--

CREATE TABLE IF NOT EXISTS `Komentarz` (
  `idKomentarza` int(11) NOT NULL AUTO_INCREMENT,
  `idPublikacji` int(11) NOT NULL,
  `idAutora` int(11) NULL,
  `dataDodania` date NOT NULL,
  `tresc` varchar(250) NOT NULL,
  `typWpisu` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`idKomentarza`),
  KEY `idPublikacji` (`idPublikacji`),
  KEY `idAutora` (`idAutora`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Prawa`
--

CREATE TABLE IF NOT EXISTS `Prawa` (
  `idPrawa` int(11) NOT NULL AUTO_INCREMENT,
  `czyDyrektor` tinyint(1) NOT NULL,
  `czyRedaktorNaczelny` tinyint(1) NOT NULL,
  PRIMARY KEY (`idPrawa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Publikacja`
--

CREATE TABLE IF NOT EXISTS `Publikacja` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `typ` varchar(30) NOT NULL,
  `idDzialu` int(11) NOT NULL,
  `idAutora` int(11) NOT NULL,
  `dataPublikacji` datetime NOT NULL,
  `akceptacja` tinyint(1) NOT NULL,
  `wstepnaAkceptacja` tinyint(1) NOT NULL,
  `idGalerii` int(11) NOT NULL,
  `tytul` varchar(50) NOT NULL,
  `tresc` varchar(21844) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idDzialu` (`idDzialu`),
  KEY `idDzialu_2` (`idDzialu`),
  KEY `idGalerii` (`idGalerii`),
  KEY `idAutora` (`idAutora`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Uzytkownik`
--

CREATE TABLE IF NOT EXISTS `Uzytkownik` (
  `idUzytkownika` int(11) NOT NULL AUTO_INCREMENT,
  `nazwaWyswietlana` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `haslo` char(32) NOT NULL,
  `idPrawa` int(11) NOT NULL,
  PRIMARY KEY (`idUzytkownika`),
  KEY `idPrawa` (`idPrawa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `UzytkownikRedaktor`
--

CREATE TABLE IF NOT EXISTS `UzytkownikRedaktor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUzytkownika` int(11) DEFAULT NULL,
  `idDzialu` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idUzytkownika` (`idUzytkownika`),
  KEY `idDzialu` (`idDzialu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `UzytkownikRedaktorDzialu`
--

CREATE TABLE IF NOT EXISTS `UzytkownikRedaktorDzialu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idUzytkownika` int(11) DEFAULT NULL,
  `idDzialu` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idUzytkownika` (`idUzytkownika`),
  KEY `idDzialu` (`idDzialu`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Zdjecie`
--

CREATE TABLE IF NOT EXISTS `Zdjecie` (
  `idZdjecie` int(11) NOT NULL AUTO_INCREMENT,
  `nazwa` varchar(30) NOT NULL,
  `opis` varchar(30) DEFAULT NULL,
  `lokalizacja` varchar(200) NOT NULL,
  `idGalerii` int(11) NOT NULL,
  PRIMARY KEY (`idZdjecie`),
  KEY `idGalerii` (`idGalerii`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Galeria`
--
ALTER TABLE `Galeria`
  ADD CONSTRAINT `Galeria_ibfk_2` FOREIGN KEY (`idAutora`) REFERENCES `Uzytkownik` (`idUzytkownika`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Galeria_ibfk_1` FOREIGN KEY (`idDzialu`) REFERENCES `Dzial` (`idDzialu`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Komentarz`
--
ALTER TABLE `Komentarz`
  ADD CONSTRAINT `Komentarz_ibfk_2` FOREIGN KEY (`idAutora`) REFERENCES `Uzytkownik` (`idUzytkownika`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `Komentarz_ibfk_1` FOREIGN KEY (`idPublikacji`) REFERENCES `Publikacja` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `Publikacja`
--
ALTER TABLE `Publikacja`
  ADD CONSTRAINT `Publikacja_ibfk_1` FOREIGN KEY (`idDzialu`) REFERENCES `Dzial` (`idDzialu`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Publikacja_ibfk_2` FOREIGN KEY (`idAutora`) REFERENCES `Uzytkownik` (`idUzytkownika`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `Uzytkownik`
--
ALTER TABLE `Uzytkownik`
  ADD CONSTRAINT `Uzytkownik_ibfk_1` FOREIGN KEY (`idPrawa`) REFERENCES `Prawa` (`idPrawa`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `UzytkownikRedaktor`
--
ALTER TABLE `UzytkownikRedaktor`
  ADD CONSTRAINT `UzytkownikRedaktor_ibfk_2` FOREIGN KEY (`idDzialu`) REFERENCES `Dzial` (`idDzialu`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `UzytkownikRedaktor_ibfk_1` FOREIGN KEY (`idUzytkownika`) REFERENCES `Uzytkownik` (`idUzytkownika`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `UzytkownikRedaktorDzialu`
--
ALTER TABLE `UzytkownikRedaktorDzialu`
  ADD CONSTRAINT `UzytkownikRedaktorDzialu_ibfk_2` FOREIGN KEY (`idDzialu`) REFERENCES `Dzial` (`idDzialu`),
  ADD CONSTRAINT `UzytkownikRedaktorDzialu_ibfk_1` FOREIGN KEY (`idUzytkownika`) REFERENCES `Uzytkownik` (`idUzytkownika`);

--
-- Constraints for table `Zdjecie`
--
ALTER TABLE `Zdjecie`
  ADD CONSTRAINT `Zdjecie_ibfk_1` FOREIGN KEY (`idGalerii`) REFERENCES `Galeria` (`idGaleria`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
