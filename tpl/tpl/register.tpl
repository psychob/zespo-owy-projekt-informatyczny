{if isset($error) && !empty($error)}
 <div class="error">
 {if $error == 'email'}
  Podany adres e-mail jest nieprawidłowy.
 {elseif $error == 'pass'}
  Podane hasła nie są identyczne.
 {elseif $error == 'already'}
  Konto o takim adresie e-mail już istnieje.
 {elseif $error == 'empty'}
  Nie wypełniono wszystkich pól
 {/if}
 </div>
{/if}

{if !isset($status)}
<form method="POST" action="index.php?strona=register&amp;step=2">
 <div>
  <b>Wyświtlana Nazwa:</b> <input type="text" name="login">
 </div>
 <div>
  <b>Hasło:</b> <input type="password" name="haslo1">
 </div>
 <div>
  <b>Powtórz hasło:</b> <input type="password" name="haslo2">
 </div>
 <div>
  <b>Email:</b> <input type="text" name="email">
 </div>
 
 <div>
  <input type="submit" value="Utwórz konto" name="rejestruj">
 </div>
</form>
{else}
 <div class="success">
  Konto zostało pomyślnie utworzone
 </div>
{/if}