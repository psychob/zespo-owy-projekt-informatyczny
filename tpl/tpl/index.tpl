<!DOCTYPE html>
<html lang="pl">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS</title>
    <link href="css/style.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>

  <body>


    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="admin/">CMS</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php?strona=index">Home</a></li>
          </ul>
         {if !$user_logged}
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php?strona=login">Zaloguj</a></li>
            <li><a href="index.php?strona=register">Zarejestruj</a></li>
          </ul>
         {else}
          <ul class="nav navbar-nav navbar-right">
            <li><a>Witaj {$user_info.nazwaWyswietlana}!</a></li>
            <li><a href="index.php?strona=logout">Wyloguj</a></li>
          </ul>
         {/if}
        </div>
      </div>
    </div>

    <div class="container">

      <div class="row">
        <div class="col-sm-8">
       
       {include file="$page.tpl"}
       
       </div>
      
        <div class="col-sm-3 col-sm-offset-1 ">         
          <div class="row sidebar-module">
            <h4>Działy</h4>
            <ol class="list-unstyled">
                {foreach $dzial as $v}
                     <li><a href="index.php?strona=dzial&id={$v.idDzialu}">{$v.nazwa}</a></li>
                {/foreach}
            </ol>
           </div>
        </div>
      </div>

    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted">Projekt zespołowy systemu informatycznego.</p>
      </div>
    </div>

  </body>
</html>
