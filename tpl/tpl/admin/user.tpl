{if $sub == "list"}
 {if isset($status)}
 <div class="success">
  {if $status == "add"}
   Pomyślnie dodano nowy dział.
  {elseif $status == "edit"}
   Pomyślnie edytowano dział
  {elseif $status == "delete"}
   Pomyślnie usunięto dział
  {/if}
 </div>
 {/if}
 <table>
  <tr>
   <th>ID</th>
   <th>Nazwa</th>
   <th>Adres E-Mail</th>
   <th>Opcje</th>
  </tr>
  
  {foreach $tab as $v}
   <tr>
    <td> {$v.idUzytkownika} </td>
    <td> {$v.nazwaWyswietlana} </td>
    <td> {$v.email} </td>
    <td>
     <a href="?what=user&amp;sub=edit&amp;id={$v.idUzytkownika}">Modyfikuj</a>
     <a href="?what=user&amp;sub=edit-priv&amp;id={$v.idUzytkownika}">Modyfikuj Uprawnienia</a>
     <a href="?what=user&amp;sub=delete&amp;id={$v.idUzytkownika}">Usuń</a>
    </td>
   </tr>
  {/foreach}
 </table>
 
 <div class="paginator">
  &lt;
  
  {for $it=0 to $max_pages}
   {if $it == $current_page}
    <b>{$it}</b>
   {else}
    <a href="?what=dzialy&amp;sub=list&amp;page={$it}">{$it}</a>
   {/if}
  {/for}
  
  &gt;
 </div>
{elseif $sub == "add"}
 <form action="?what=user&amp;sub=add-submit" method="POST">
  <div>
   <b>Wyświtlana Nazwa:</b> <input type="text" name="login">
  </div>
  <div>
   <b>Hasło:</b> <input type="password" name="haslo1">
  </div>
  <div>
   <b>Email:</b> <input type="text" name="email">
  </div>
  <div>
   <b>Uprawnienia:</b>
   <select name="rights">
    <option value="1">Śmiertelnik</option>
    <option value="3">Redaktor Naczelny</option>
    <option value="2">Dyrektor</option>
   </select>
  </div>
  <div>
   <input type="submit" value="Dodaj Użytkownika" />
  </div>
 </form>
{elseif $sub == "edit"}
 <form action="?what=user&amp;sub=edit-submit" method="POST">
  <div>
   <b>Wyświtlana Nazwa:</b> <input type="text" name="login" value="{$item.nazwaWyswietlana}">
  </div>
  <div>
   <b>Hasło:</b> <input type="password" name="haslo1" placeholder="Modyfikuj tylko jeśli chcesz to zmienić!">
  </div>
  <div>
   <b>Email:</b> <input type="text" name="email" value="{$item.email}">
  </div>
  <div>
   <b>Uprawnienia:</b>
   <select name="rights">
    <option value="1" {if $item.idPrawa == 1}selected="selected"{/if}>Śmiertelnik</option>
    <option value="3" {if $item.idPrawa == 3}selected="selected"{/if}>Redaktor Naczelny</option>
    <option value="2" {if $item.idPrawa == 2}selected="selected"{/if}>Dyrektor</option>
   </select>
  </div>
  <div>
   <input type="submit" value="Modyfikuj Użytkownika" />
   <input type="hidden" name="id" value="{$item.idUzytkownika}" />
  </div>
 </form>
{elseif $sub == "mod-priv"}
 <div>
  Użytkownik: <b>{$user_name}</b>
 </div>
  
  <fieldset>
   <legend>Uprawnienia Redaktora</legend>
   
   <ul>
    {foreach $red_rights as $v}
     <li>
      {$v.name} - <a href="?what=user&amp;sub=revoke-priv-r&amp;uid={$user_id}&amp;did={$v.id}">Odwołaj</a>
     </li>
    {foreachelse}
     <li>Podany użytkownik nie jest redaktorem!</li>
    {/foreach}
   </ul>
  </fieldset>
 
  <fieldset>
   <legend>Uprawnienia Redaktora Działu</legend>
   
   <ul>
    {foreach $redd_rights as $v}
     <li>
      {$v.name} - <a href="?what=user&amp;sub=revoke-priv-rd&amp;uid={$user_id}&amp;did={$v.id}">Odwołaj</a>
     </li>
    {foreachelse}
     <li>Podany użytkownik nie jest redaktorem działu!</li>
    {/foreach}
   </ul>
  </fieldset>
{/if}