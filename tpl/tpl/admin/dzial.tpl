{if $sub == "list"}
 {if isset($status)}
 <div class="success">
  {if $status == "add"}
   Pomyślnie dodano nowy dział.
  {elseif $status == "edit"}
   Pomyślnie edytowano dział
  {elseif $status == "delete"}
   Pomyślnie usunięto dział
  {/if}
 </div>
 {/if}
 <table>
  <tr>
   <th>ID</th>
   <th>Nazwa</th>
   <th>Opiekun</th>
   <th>Opcje</th>
  </tr>
  
  {foreach $tab as $v}
   <tr>
    <td> {$v.idDzialu} </td>
    <td> {$v.nazwa} </td>
    <td> {$v.nazwaWyswietlana} </td>
    <td> <a href="?what=dzialy&amp;sub=edit&amp;id={$v.idDzialu}">Modyfikuj</a> <a href="?what=dzialy&amp;sub=delete&amp;id={$v.idDzialu}">Usuń</a> </td>
   </tr>
  {/foreach}
 </table>
 
 <div class="paginator">
  &lt;
  
  {for $it=0 to $max_pages}
   {if $it == $current_page}
    <b>{$it}</b>
   {else}
    <a href="?what=dzialy&amp;sub=list&amp;page={$it}">{$it}</a>
   {/if}
  {/for}
  
  &gt;
 </div>
{elseif $sub == "add"}
 <form action="?what=dzialy&amp;sub=add-submit" method="POST">
  <div>
   Nazwa: <input name="nazwa" maxlength="30" required="required"/>
  </div>
  <div>
   Redaktor Działu:
   <select name="redaktor">
    {foreach $tab as $v}
     <option value="{$v.idUzytkownika}">{$v.nazwaWyswietlana}</option>
    {/foreach}
   </select>
  </div>
  <div>
   <input type="submit" value="Dodaj Dział" />
  </div>
 </form>
{elseif $sub == "edit"}
 <form action="?what=dzialy&amp;sub=edit-submit" method="POST">
  <div>
   Nazwa: <input name="nazwa" maxlength="30" required="required" value="{$item.nazwa}"/>
  </div>
  <div>
   Redaktor Działu:
   <select name="redaktor">
    {foreach $tab as $v}
     <option value="{$v.idUzytkownika}"{if $item.idRedaktora == $v.idUzytkownika} selected="selected"{/if}>{$v.nazwaWyswietlana}</option>
    {/foreach}
   </select>
  </div>
  <div>
   <input type="submit" value="Edytuj Dział" />
   <input type="hidden" name="id" value="{$item.idDzialu}" />
  </div>
 </form>
{/if}