{if $sub == "list"}
 <table>
  <tr>
   <th> ID </th>
   <th> Nazwa Działu </th>
  </tr>
  
  {foreach $tab as $v}
   <tr>
    <td> {$v.id} </td>
    <td> <a href="?what=dzialcp&amp;sub=list-users&amp;did={$v.id}"> {$v.name} </a> </td>
   </tr>
  {/foreach}
 </table>
{elseif $sub == "list-user"}
 <table>
  <tr>
   <th> ID </th>
   <th> Nazwa Użytkownika </th>
   <th> Opcje </th>
  </tr>
  
  {foreach $tab as $v}
   <tr>
    <td> {$v.idUzytkownika} </td>
    <td> {$v.nazwaWyswietlana} </td>
    <td> <a href="?what=dzialcp&amp;sub=revoke&amp;uid={$v.idUzytkownika}&amp;did={$did}"> Zabierz uprawnienie redaktora </a> </td>
   </tr>
  {/foreach}
  
 </table>
  
  <a href="?what=dzialcp&amp;sub=add-user&amp;did={$did}">Dodaj Redaktora</a>
{elseif $sub == "add-user"}
 <form action="?what=dzialcp&amp;sub=add-user-submit" method="POST">
  <div>
   Nazwa Użytkownika:
   <select name="user-id">
    {foreach $tab as $v}
     <option value="{$v.idUzytkownika}">{$v.nazwaWyswietlana}</option>
    {/foreach}
   </select>
  </div>
   
  <div>
   <input type="submit" value="Dodaj Redaktora" />
   <input type="hidden" value="{$did}" name="did" />
  </div>
 </form>
{/if}