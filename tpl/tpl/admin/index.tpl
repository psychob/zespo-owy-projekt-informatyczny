<!doctype html>
<html>
 <head>
  <title>Panel Administracyjny</title>
  <meta charset="utf-8" />
  <link href="../css/adminstyle.css" rel="stylesheet">
 </head>
 <body>
  <h1>Panel Administracyjny</h1>
 
  
  <ul>
   {if $prawa_dzial}
   <li>
    <a href="?what=dzialy&amp;sub=list">Działy</a>
    <ul>
     <li> <a href="?what=dzialy&amp;sub=add">Dodaj</a> </li>
     <li> <a href="?what=dzialy&amp;sub=list">Wyświetl</a> </li>
    </ul>
   </li>
   {/if}
   
   {if $prawa_user}
   <li>
    <a href="?what=user&amp;sub=list">Użyszkodnicy</a>
    <ul>
     <li> <a href="?what=user&amp;sub=add">Dodaj</a> </li>
     <li> <a href="?what=user&amp;sub=list">Wyświetl</a> </li>
    </ul>
   </li>
   {/if}
   
   {if $prawa_publikacja}
   <li>
    <a href="?what=publikacja&amp;sub=list">Publikacja</a>
    <ul>
     <li> <a href="?what=publikacja&amp;sub=add">Dodaj</a> </li>
     <li> <a href="?what=publikacja&amp;sub=list">Wyświetl</a> </li>
    </ul>
   </li>
   {/if}
   
   {if $prawa_publikacja}
   <li>
    <a href="?what=galeria&amp;sub=list">Galeria</a>
    <ul>
     <li> <a href="?what=galeria&amp;sub=add">Dodaj</a> </li>
     <li> <a href="?what=galeria&amp;sub=list">Wyświetl</a> </li>
    </ul>
   </li>
   {/if}
   
   {if $prawa_akceptacja}
   <li>
    <a href="?what=manage">Akceptuj treści</a>
   </li>
   {/if}
   
   {if $prawa_dzialcp}
   <li>
    <a href="?what=dzialcp">Zarządzaj Działem</a>
   </li>
   {/if}
   <li>
    <a href="../">Wróć do strony</a>
   </li>
  </ul>
  <div id="content">
  {include file="$page.tpl"}
  </div>
   
 </body> 
</html>