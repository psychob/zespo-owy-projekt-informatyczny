{if $sub == "list"}
 {if isset($status)}
 <div class="success">
  {if $status == "add"}
   Pomyślnie dodano nową galerię.
  {elseif $status == "edit"}
   Pomyślnie edytowano galerię
  {elseif $status == "delete"}
   Pomyślnie usunięto galerię
  {/if}
 </div>
 {/if}
 <table>
  <tr>
   <th>ID</th>
   <th>Nazwa</th>
   <th>Autor</th>
  </tr>
  
  {foreach $tab as $v}
   <tr>
    <td> {$v.idGaleria} </td>
    <td> {$v.nazwa} </td>
    <td> {$v.nazwaWyswietlana} </td>
    <td> <a href="?what=galeria&amp;sub=edit&amp;id={$v.id}">Modyfikuj</a> <a href="?what=galeria&amp;sub=delete&amp;id={$v.id}">Usuń</a> </td>
   </tr>
  {/foreach}
 </table>
 
 <div class="paginator">
  &lt;
  
  {for $it=0 to $max_pages}
   {if $it == $current_page}
    <b>{$it}</b>
   {else}
    <a href="?what=galeria&amp;sub=list&amp;page={$it}">{$it}</a>
   {/if}
  {/for}
  
  &gt;
 </div>
{elseif $sub == "add"} 
<form method="post" action="?what=galeria&amp;sub=add-submit">
    <div>
        Nazwa: <input type="text" name="nazwa" value="" required>
    </div>
     <div>
        Dodaj zdjecia z bazy(max 5): 
        {foreach $tab as $v}
                 <option value="{$v.idZdjecie}">{$v.nazwa}</option>
                 <option value="{$v.idZdjecie}">{$v.nazwa}</option>
                 <option value="{$v.idZdjecie}">{$v.nazwa}</option>
                 <option value="{$v.idZdjecie}">{$v.nazwa}</option>
                 <option value="{$v.idZdjecie}">{$v.nazwa}</option>               
            {/foreach}
    </div>
    <div>
        Dział: <select name="dzial" required>
            {foreach $tab as $v}
                 <option value="{$v.idDzialu}">{$v.nazwa}</option>
            {/foreach}
        </select>
    </div>
    <div>
        Opis:<br>		
        <textarea rows="20" cols="100" name="opis" required></textarea><br>
    </div>
    <div>
        <input type="submit" value="Dodaj Galerię" />
    </div>
</form>  
{elseif $sub == "edit"}
<form method="post" action="?what=publikacja&amp;sub=edit-submit">
    <div>
        Nazwa: <input type="text" name="nazwa" value="{$item.nazwa}" required>
    </div>
    <div>
        Dział: <select name="dzial" required>
            {foreach $tab as $v}
                 <option value="{$v.idDzialu}" {if $item.idDzialu == $v.idDzialu} selected="selected"{/if}>{$v.nazwa}</option>
            {/foreach}
        </select>
    </div>      
    <div>
        Opis:<br>		
        <textarea rows="20" cols="100" name="opis" required>{$item.opis}</textarea><br>
    </div>
    <div>
        <input type="submit" value="Edytuj Galerię" />
        <input type="hidden" name="id" value="{$item.id}" />
    </div>
</form>
{/if}