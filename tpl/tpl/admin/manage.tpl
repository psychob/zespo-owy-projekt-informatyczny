<h1>Zatwierdzanie artykułów</h1>

<table width="100%">
    <tr>
        <th><a href="?what=manage&amp;action=print&amp;orderby=id&amp;descend={$descend}">ID</a></th>
        <th><a href="?what=manage&amp;action=print&amp;orderby=tytul&amp;descend={$descend}">Tytul</a></th>
        <th><a href="?what=manage&amp;action=print&amp;orderby=nazwaWyswietlana&amp;descend={$descend}">Użytkownik</a></th>
        <th><a href="?what=manage&amp;action=print&amp;orderby=dataPublikacji&amp;descend={$descend}">Data publikacji</a></th>
        <th><a href="?what=manage&amp;action=print&amp;orderby=akceptacja&amp;descend={$descend}">Dział</a></th>
        <th>Opcje</th>
    </tr>  
    {foreach $tab as $v}
        <tr>
            <td> {$v.id} </td>
            <td> {$v.tytul} </td>
            <td> {$v.nazwaWyswietlana} </td>
            <td> {$v.dataPublikacji} </td>
            <td> {$v.nazwa} </td>
            {if ($v.akceptacja) == 0}
                <td> <a href="?what=manage&amp;action=manage&amp;sub=accept&amp;id={$v.id}">Zatwierdź</a></td>    
            {else}
                <td> <a href="?what=manage&amp;action=manage&amp;sub=cancelAccept&amp;id={$v.id}">Anuluj akceptacje</a></td>    
            {/if}
        </tr>
    {/foreach}
</table>

