{if $sub == "list"}
 {if isset($status)}
 <div class="success">
  {if $status == "add"}
   Pomyślnie dodano nową publikację.
  {elseif $status == "edit"}
   Pomyślnie edytowano publikację
  {elseif $status == "delete"}
   Pomyślnie usunięto publikację
  {/if}
 </div>
 {/if}
 <table>
  <tr>
   <th>ID</th>
   <th>Tytul</th>
   <th>Autor</th>
   <th>Opcje</th>
  </tr>
  
  {foreach $tab as $v}
   <tr>
    <td> {$v.id} </td>
    <td> {$v.tytul} </td>
    <td> {$v.nazwaWyswietlana} </td>
    <td> <a href="?what=publikacja&amp;sub=edit&amp;id={$v.id}">Modyfikuj</a> <a href="?what=publikacja&amp;sub=delete&amp;id={$v.id}">Usuń</a> </td>
   </tr>
  {/foreach}
 </table>
 
 <div class="paginator">
  &lt;
  
  {for $it=0 to $max_pages}
   {if $it == $current_page}
    <b>{$it}</b>
   {else}
    <a href="?what=publikacja&amp;sub=list&amp;page={$it}">{$it}</a>
   {/if}
  {/for}
  
  &gt;
 </div>
{elseif $sub == "add"} 
<form method="post" action="?what=publikacja&amp;sub=add-submit">
    <div>
        Tytuł: <input type="text" name="tytul" value="" required>
    </div>
    <div>
        Typ: <select name="typ" required>
        <option value="news">News</option>
        <option value="article">Artykuł</option>
        </select>
    </div>
    <div>
        Dział: <select name="dzial" required>
            {foreach $tab as $v}
                 <option value="{$v.idDzialu}">{$v.nazwa}</option>
            {/foreach}
        </select>
    </div>
    <div>
        Galeria: <select name="galeria" required>
            {foreach $tab2 as $v}
                 <option value="{$v.idGaleria}">{$v.nazwa}</option>
            {/foreach}
        </select>
    </div>
    <div>
        Treść:<br>		
        <textarea rows="20" cols="100" name="tresc" required></textarea><br>
    </div>
    <div>
        <input type="submit" value="Dodaj Publikację" />
    </div>
</form>  
{elseif $sub == "edit"}
<form method="post" action="?what=publikacja&amp;sub=edit-submit">
    <div>
        Tytuł: <input type="text" name="tytul" value="{$item.tytul}" required>
    </div>
    <div>
        Typ: <select name="typ" required>
        <option value="news"{if $item.typ == "news"} selected="selected"{/if}>News</option>
        <option value="article" {if $item.typ == "article"} selected="selected"{/if}>Artykuł</option>
        </select>
    </div>
    <div>
        Dział: <select name="dzial" required>
            {foreach $tab as $v}
                 <option value="{$v.idDzialu}" {if $item.idDzialu == $v.idDzialu} selected="selected"{/if}>{$v.nazwa}</option>
            {/foreach}
        </select>
    </div>      
    <div>
        Treść:<br>		
        <textarea rows="20" cols="100" name="tresc" required>{$item.tresc}</textarea><br>
    </div>
    <div>
        <input type="submit" value="Edytuj Publikację" />
        <input type="hidden" name="id" value="{$item.id}" />
    </div>
</form>
{/if}