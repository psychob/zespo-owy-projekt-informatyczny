<?php /* Smarty version Smarty-3.1.18, created on 2014-06-01 07:57:03
         compiled from "..\tpl\tpl\admin\publikacja.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3381538abd18bd7836-07186007%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '914acea496faf7af5333dbc84d4fc651f42da399' => 
    array (
      0 => '..\\tpl\\tpl\\admin\\publikacja.tpl',
      1 => 1401601748,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3381538abd18bd7836-07186007',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_538abd18c874e8_21436591',
  'variables' => 
  array (
    'sub' => 0,
    'status' => 0,
    'tab' => 0,
    'v' => 0,
    'max_pages' => 0,
    'it' => 0,
    'current_page' => 0,
    'tab2' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_538abd18c874e8_21436591')) {function content_538abd18c874e8_21436591($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['sub']->value=="list") {?>
 <?php if (isset($_smarty_tpl->tpl_vars['status']->value)) {?>
 <div class="success">
  <?php if ($_smarty_tpl->tpl_vars['status']->value=="add") {?>
   Pomyślnie dodano nową publikację.
  <?php } elseif ($_smarty_tpl->tpl_vars['status']->value=="edit") {?>
   Pomyślnie edytowano publikację
  <?php } elseif ($_smarty_tpl->tpl_vars['status']->value=="delete") {?>
   Pomyślnie usunięto publikację
  <?php }?>
 </div>
 <?php }?>
 <table>
  <tr>
   <th>ID</th>
   <th>Tytul</th>
   <th>Autor</th>
  </tr>
  
  <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tab']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
   <tr>
    <td> <?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
 </td>
    <td> <?php echo $_smarty_tpl->tpl_vars['v']->value['tytul'];?>
 </td>
    <td> <?php echo $_smarty_tpl->tpl_vars['v']->value['nazwaWyswietlana'];?>
 </td>
    <td> <a href="?what=publikacja&amp;sub=edit&amp;id=<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
">Modyfikuj</a> <a href="?what=publikacja&amp;sub=delete&amp;id=<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
">Usuń</a> </td>
   </tr>
  <?php } ?>
 </table>
 
 <div class="paginator">
  &lt;
  
  <?php $_smarty_tpl->tpl_vars['it'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['it']->step = 1;$_smarty_tpl->tpl_vars['it']->total = (int) ceil(($_smarty_tpl->tpl_vars['it']->step > 0 ? $_smarty_tpl->tpl_vars['max_pages']->value+1 - (0) : 0-($_smarty_tpl->tpl_vars['max_pages']->value)+1)/abs($_smarty_tpl->tpl_vars['it']->step));
if ($_smarty_tpl->tpl_vars['it']->total > 0) {
for ($_smarty_tpl->tpl_vars['it']->value = 0, $_smarty_tpl->tpl_vars['it']->iteration = 1;$_smarty_tpl->tpl_vars['it']->iteration <= $_smarty_tpl->tpl_vars['it']->total;$_smarty_tpl->tpl_vars['it']->value += $_smarty_tpl->tpl_vars['it']->step, $_smarty_tpl->tpl_vars['it']->iteration++) {
$_smarty_tpl->tpl_vars['it']->first = $_smarty_tpl->tpl_vars['it']->iteration == 1;$_smarty_tpl->tpl_vars['it']->last = $_smarty_tpl->tpl_vars['it']->iteration == $_smarty_tpl->tpl_vars['it']->total;?>
   <?php if ($_smarty_tpl->tpl_vars['it']->value==$_smarty_tpl->tpl_vars['current_page']->value) {?>
    <b><?php echo $_smarty_tpl->tpl_vars['it']->value;?>
</b>
   <?php } else { ?>
    <a href="?what=publikacja&amp;sub=list&amp;page=<?php echo $_smarty_tpl->tpl_vars['it']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['it']->value;?>
</a>
   <?php }?>
  <?php }} ?>
  
  &gt;
 </div>
<?php } elseif ($_smarty_tpl->tpl_vars['sub']->value=="add") {?> 
<form method="post" action="?what=publikacja&amp;sub=add-submit">
    <div>
        Tytuł: <input type="text" name="tytul" value="" required>
    </div>
    <div>
        Typ: <select name="typ" required>
        <option value="news">News</option>
        <option value="article">Artykuł</option>
        </select>
    </div>
    <div>
        Dział: <select name="dzial" required>
            <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tab']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
                 <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['idDzialu'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['nazwa'];?>
</option>
            <?php } ?>
        </select>
    </div>
    <div>
        Galeria: <select name="galeria" required>
            <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tab2']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
                 <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['idGaleria'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['nazwa'];?>
</option>
            <?php } ?>
        </select>
    </div>
    <div>
        Treść:<br>		
        <textarea rows="20" cols="100" name="tresc" required></textarea><br>
    </div>
    <div>
        <input type="submit" value="Dodaj Publikację" />
    </div>
</form>  
<?php } elseif ($_smarty_tpl->tpl_vars['sub']->value=="edit") {?>
<form method="post" action="?what=publikacja&amp;sub=edit-submit">
    <div>
        Tytuł: <input type="text" name="tytul" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['tytul'];?>
" required>
    </div>
    <div>
        Typ: <select name="typ" required>
        <option value="news"<?php if ($_smarty_tpl->tpl_vars['item']->value['typ']=="news") {?> selected="selected"<?php }?>>News</option>
        <option value="article" <?php if ($_smarty_tpl->tpl_vars['item']->value['typ']=="article") {?> selected="selected"<?php }?>>Artykuł</option>
        </select>
    </div>
    <div>
        Dział: <select name="dzial" required>
            <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tab']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
                 <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['idDzialu'];?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value['idDzialu']==$_smarty_tpl->tpl_vars['v']->value['idDzialu']) {?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['v']->value['nazwa'];?>
</option>
            <?php } ?>
        </select>
    </div>      
    <div>
        Treść:<br>		
        <textarea rows="20" cols="100" name="tresc" required><?php echo $_smarty_tpl->tpl_vars['item']->value['tresc'];?>
</textarea><br>
    </div>
    <div>
        <input type="submit" value="Edytuj Publikację" />
        <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
" />
    </div>
</form>
<?php }?><?php }} ?>
