<?php /* Smarty version Smarty-3.1.18, created on 2014-06-01 09:27:27
         compiled from "..\tpl\tpl\admin\galeria.tpl" */ ?>
<?php /*%%SmartyHeaderCode:13569538ad417d8bef7-62550618%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '261c6540ea253c06f2a9588c899353778e515e6c' => 
    array (
      0 => '..\\tpl\\tpl\\admin\\galeria.tpl',
      1 => 1401607443,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13569538ad417d8bef7-62550618',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_538ad417e62ca5_57475646',
  'variables' => 
  array (
    'sub' => 0,
    'status' => 0,
    'tab' => 0,
    'v' => 0,
    'max_pages' => 0,
    'it' => 0,
    'current_page' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_538ad417e62ca5_57475646')) {function content_538ad417e62ca5_57475646($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['sub']->value=="list") {?>
 <?php if (isset($_smarty_tpl->tpl_vars['status']->value)) {?>
 <div class="success">
  <?php if ($_smarty_tpl->tpl_vars['status']->value=="add") {?>
   Pomyślnie dodano nową galerię.
  <?php } elseif ($_smarty_tpl->tpl_vars['status']->value=="edit") {?>
   Pomyślnie edytowano galerię
  <?php } elseif ($_smarty_tpl->tpl_vars['status']->value=="delete") {?>
   Pomyślnie usunięto galerię
  <?php }?>
 </div>
 <?php }?>
 <table>
  <tr>
   <th>ID</th>
   <th>Nazwa</th>
   <th>Autor</th>
  </tr>
  
  <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tab']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
   <tr>
    <td> <?php echo $_smarty_tpl->tpl_vars['v']->value['idGaleria'];?>
 </td>
    <td> <?php echo $_smarty_tpl->tpl_vars['v']->value['nazwa'];?>
 </td>
    <td> <?php echo $_smarty_tpl->tpl_vars['v']->value['nazwaWyswietlana'];?>
 </td>
    <td> <a href="?what=galeria&amp;sub=edit&amp;id=<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
">Modyfikuj</a> <a href="?what=galeria&amp;sub=delete&amp;id=<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
">Usuń</a> </td>
   </tr>
  <?php } ?>
 </table>
 
 <div class="paginator">
  &lt;
  
  <?php $_smarty_tpl->tpl_vars['it'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['it']->step = 1;$_smarty_tpl->tpl_vars['it']->total = (int) ceil(($_smarty_tpl->tpl_vars['it']->step > 0 ? $_smarty_tpl->tpl_vars['max_pages']->value+1 - (0) : 0-($_smarty_tpl->tpl_vars['max_pages']->value)+1)/abs($_smarty_tpl->tpl_vars['it']->step));
if ($_smarty_tpl->tpl_vars['it']->total > 0) {
for ($_smarty_tpl->tpl_vars['it']->value = 0, $_smarty_tpl->tpl_vars['it']->iteration = 1;$_smarty_tpl->tpl_vars['it']->iteration <= $_smarty_tpl->tpl_vars['it']->total;$_smarty_tpl->tpl_vars['it']->value += $_smarty_tpl->tpl_vars['it']->step, $_smarty_tpl->tpl_vars['it']->iteration++) {
$_smarty_tpl->tpl_vars['it']->first = $_smarty_tpl->tpl_vars['it']->iteration == 1;$_smarty_tpl->tpl_vars['it']->last = $_smarty_tpl->tpl_vars['it']->iteration == $_smarty_tpl->tpl_vars['it']->total;?>
   <?php if ($_smarty_tpl->tpl_vars['it']->value==$_smarty_tpl->tpl_vars['current_page']->value) {?>
    <b><?php echo $_smarty_tpl->tpl_vars['it']->value;?>
</b>
   <?php } else { ?>
    <a href="?what=galeria&amp;sub=list&amp;page=<?php echo $_smarty_tpl->tpl_vars['it']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['it']->value;?>
</a>
   <?php }?>
  <?php }} ?>
  
  &gt;
 </div>
<?php } elseif ($_smarty_tpl->tpl_vars['sub']->value=="add") {?> 
<form method="post" action="?what=galeria&amp;sub=add-submit">
    <div>
        Nazwa: <input type="text" name="nazwa" value="" required>
    </div>
    <div>
        Dział: <select name="dzial" required>
            <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tab']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
                 <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['idDzialu'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['nazwa'];?>
</option>
            <?php } ?>
        </select>
    </div>
    <div>
        Opis:<br>		
        <textarea rows="20" cols="100" name="opis" required></textarea><br>
    </div>
    <div>
        <input type="submit" value="Dodaj Galerię" />
    </div>
</form>  
<?php } elseif ($_smarty_tpl->tpl_vars['sub']->value=="edit") {?>
<form method="post" action="?what=publikacja&amp;sub=edit-submit">
    <div>
        Nazwa: <input type="text" name="nazwa" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['nazwa'];?>
" required>
    </div>
    <div>
        Dział: <select name="dzial" required>
            <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['tab']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
                 <option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['idDzialu'];?>
" <?php if ($_smarty_tpl->tpl_vars['item']->value['idDzialu']==$_smarty_tpl->tpl_vars['v']->value['idDzialu']) {?> selected="selected"<?php }?>><?php echo $_smarty_tpl->tpl_vars['v']->value['nazwa'];?>
</option>
            <?php } ?>
        </select>
    </div>      
    <div>
        Opis:<br>		
        <textarea rows="20" cols="100" name="opis" required><?php echo $_smarty_tpl->tpl_vars['item']->value['opis'];?>
</textarea><br>
    </div>
    <div>
        <input type="submit" value="Edytuj Galerię" />
        <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['id'];?>
" />
    </div>
</form>
<?php }?><?php }} ?>
