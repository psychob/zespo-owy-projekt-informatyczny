<?php /* Smarty version Smarty-3.1.18, created on 2014-06-01 07:33:07
         compiled from ".\tpl\tpl\index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:24259538abb13749284-19889148%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '483e20f4708fa945b6aa6082806e2fcfab9dc1db' => 
    array (
      0 => '.\\tpl\\tpl\\index.tpl',
      1 => 1401359782,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '24259538abb13749284-19889148',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'user_logged' => 0,
    'user_info' => 0,
    'dzial' => 0,
    'v' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.18',
  'unifunc' => 'content_538abb137ba713_28641896',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_538abb137ba713_28641896')) {function content_538abb137ba713_28641896($_smarty_tpl) {?><!DOCTYPE html>
<html lang="pl">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>CMS</title>
    <link href="css/style.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>

  <body>


    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="admin/">CMS</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php?strona=index">Home</a></li>
            
          </ul>
         <?php if (!$_smarty_tpl->tpl_vars['user_logged']->value) {?>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php?strona=login">Zaloguj</a></li>
            <li><a href="index.php?strona=register">Zarejestruj</a></li>
          </ul>
         <?php } else { ?>
          <ul class="nav navbar-nav navbar-right">
            <li><a>Witaj <?php echo $_smarty_tpl->tpl_vars['user_info']->value['nazwaWyswietlana'];?>
!</a></li>
            <li><a href="index.php?strona=logout">Wyloguj</a></li>
          </ul>
         <?php }?>
        </div>
      </div>
    </div>

    <div class="container">

      <div class="row">
        <div class="col-sm-8">
       
       <?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['page']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>

       
       </div>
      
        <div class="col-sm-3 col-sm-offset-1 ">         
          <div class="row sidebar-module">
            <h4>Działy</h4>
            <ol class="list-unstyled">
                <?php  $_smarty_tpl->tpl_vars['v'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['v']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['dzial']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['v']->key => $_smarty_tpl->tpl_vars['v']->value) {
$_smarty_tpl->tpl_vars['v']->_loop = true;
?>
                     <li><a href="index.php?strona=dzial&id=<?php echo $_smarty_tpl->tpl_vars['v']->value['idDzialu'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['nazwa'];?>
</a></li>
                <?php } ?>
            </ol>
           </div>
        </div>
      </div>

    </div>

    <div id="footer">
      <div class="container">
        <p class="text-muted">Projekt zespołowy systemu informatycznego.</p>
      </div>
    </div>

  </body>
</html>
<?php }} ?>
