<?php
 $theme->assign('page', 'login');
 
 if ( isset($_GET['step']) && $_GET['step'] == 2 )
 {
  // sprawdzamy czy mamy wszystko wypełnione
  if ( isset($_POST['login']) && !empty($_POST['login']) && 
       isset($_POST['haslo']) && !empty($_POST['haslo']) )
  {
   $error = $user->zaloguj( $_POST['login'], $_POST['haslo'] );
   
   if ( $error == "ok" )
    header('location: index.php?strona=login&status=ok');
   else
    header('location: index.php?strona=login&error='.$error);
   
   die;
  } else
  {
   header('location: index.php?strona=login&error=empty');
   die;
  }
 }
 
 if ( isset($_GET['error']) && !empty($_GET['error']))
  $theme->assign('error', $_GET['error']);
 
 if ( isset($_GET['status']) && $_GET['status'] == 'ok' )
  $theme->assign('status', 'ok');