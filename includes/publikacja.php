<?php

class Publikacja 
{
    private $_db = null;
    private $_dane = [];

    public function __construct(MySQL $db)
    {
     $this->_db = $db;
    } 
    
    public function dodajPublikacje($typ, $idDzialu, $idAutora, $akceptacja, $wstepnaAkceptacja, $idGalerii, $tresc, $tytul) 
    {
        $tresc = $this->_db->escapeString($tresc);
        $tytul = $this->_db->escapeString($tytul);
        
        
        $str = "INSERT INTO Publikacja (typ, idDzialu, idAutora, dataPublikacji, akceptacja, wstepnaAkceptacja, idGalerii, tresc, tytul) 
		VALUES ('$typ', '$idDzialu', '$idAutora', now(), '$akceptacja',"
                . "'$wstepnaAkceptacja', '$idGalerii', '$tresc', '$tytul')";
        
        $sql = $this->_db->query($str);
    }

    public function edytujPublikacje($typ, $idDzialu, $idGalerii, $tresc, $tytul, $id)
    {
        $tresc = $this->_db->escapeString($tresc);
        $tytul = $this->_db->escapeString($tytul);
        
	$str = "UPDATE Publikacja SET typ = '$typ', idDzialu = '$idDzialu', idGalerii = '$idGalerii', tresc = '$tresc', tytul = '$tytul' WHERE id='$id'";
	$sql = $this->_db->query($str);
    }

    public function usunPublikacje($id)
    {
	$str = "DELETE FROM Publikacja WHERE id = '$id' ";
        $sql = $this->_db->query($str);
    }

    public function pobierzPublikacje($id)
    {
	$str = "SELECT *,`nazwaWyswietlana` FROM Publikacja inner join `Uzytkownik` on (`idAutora` = `idUzytkownika`) WHERE id = '$id'";
        $this->_dane = $this->_db->query($str);
        
        if ( $this->_dane->count() )
        {
            return $this->_dane = $this->_dane->fetchAll ();
            $this->_dane = $this->_dane[0];
        } else
            $this->_dane = [];
        
        return !empty($this->_dane);
    }

    public function pobierzListePublikacji()
    {
	$str = "SELECT id, tytul FROM Publikacja";
        $this->_dane = $this->_db->query($str);
        
        if ( $this->_dane->count() )
        {
            $this->_dane = $this->_dane->fetchAll ();
            $this->_dane = $this->_dane[0];
        } else
            $this->_dane = [];
        
        return !empty($this->_dane);
        
    }
    
    public function pobierzListePublikacjizAutorem($typ, $dzial = null, $tylkoZaak = true)
    {
     $str = "select `Publikacja`.`id` as 'pub_id', `Publikacja`.`tytul` as 'pub_title', ".
            "`Publikacja`.`tresc` as 'pub_tresc', `Uzytkownik`.`idUzytkownika` as 'user_id', ".
            "`Uzytkownik`.`nazwaWyswietlana` as 'user_name', `Publikacja`.`dataPublikacji` as 'pub_date' ".
            "from `Publikacja` inner join `Uzytkownik` on (`Publikacja`.`idAutora` = `Uzytkownik`.`idUzytkownika`) ".
            "where ".
            "`Publikacja`.`typ` = '{$typ}'";
            
     if ( $tylkoZaak )
      $str .= " and `Publikacja`.`akceptacja` = 1 ";
     
     if ( !is_null($dzial) )
      $str .= " and `Publikacja`.`idDzialu` = {$dzial} ";
            
     $str .= " order by `Publikacja`.`dataPublikacji` desc;";
     $str = $this->_db->query($str);
     
     return $str->fetchAll();
    }
}
