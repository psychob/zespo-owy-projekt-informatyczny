<?php

 class Prawa
 {
  private $czyDyrektor = false;
  private $czyNaczelny = false;
  public $redaktor1 = [];
  public $redaktor2 = [];
  
  public function __construct($userId, MySQL $db)
  {
   if ( $userId == 0 )
    return ;
   
   // pobieramy prawa
   $sql = "select czyDyrektor, czyRedaktorNaczelny from Uzytkownik inner join Prawa on ( Prawa.idPrawa = Uzytkownik.idPrawa ) where idUzytkownika = {$userId};";
   $sql = $db->query($sql);
   
   $this->czyDyrektor = $sql['czyDyrektor'] == 1;
   $this->czyNaczelny = $sql['czyRedaktorNaczelny'] == 1;
   
   // pobieramy redaktorow
   $sql = "select idDzialu from UzytkownikRedaktor where idUzytkownika = {$userId};";
   $sql = $db->query($sql);
   
   $sql = $sql->fetchAll();
   foreach ( $sql as $v )
    $this->redaktor1[] = $v['idDzialu'];
   
   // pobieramy redaktorow dzialu
   $sql = "select idDzialu from UzytkownikRedaktorDzialu where idUzytkownika = {$userId};";
   $sql = $db->query($sql);
   
   $sql = $sql->fetchAll();
   foreach ( $sql as $v )
    $this->redaktor2[] = $v['idDzialu'];
  }
  
  public function czyJestemDyrektorem( )
  {
   return $this->czyDyrektor;
  }
  
  public function czyJestemRedaktoremNaczelnym()
  {
   return $this->czyNaczelny;
  }
  
  public function czyJestemRedaktorem( $idDzialu )
  {
   if ( $idDzialu == null )
    return !empty($this->redaktor1);
   
   foreach ($this->redaktor1 as $v)
    if ($v == $idDzialu)
     return true;
    
   return false;
  }
  
  public function czyJestemRedaktoremDzialu( $idDzialu )
  {
   if ( $idDzialu == null )
    return !empty($this->redaktor2);
   
   foreach ($this->redaktor2 as $v)
    if ($v == $idDzialu)
     return true;
    
   return false;
  }
 }