<?php

class Galeria 
{
    private $_db = null;
    private $_dane = [];

    public function __construct(MySQL $db)
    {
     $this->_db = $db;
    } 
    
    public function dodajGalerie($idGaleria, $nazwa, $opis, $idDzialu, $idAutora) 
    {
        $nazwa = $this->_db->escapeString($nazwa);
        $opis = $this->_db->escapeString($opis);
        
        
        $str = "INSERT INTO Galeria (idGaleria, nazwa, opis, idDzaialu, idAutora) 
		VALUES ('$idGaleria', '$nazwa', '$opis', '$idDzialu',"
                . "'$idAutora')";
        
        $sql = $this->_db->query($str);
    }

    public function edytujGalerie($idGaleria, $nazwa, $opis, $idDzialu, $idAutora)
    {
        $nazwa = $this->_db->escapeString($nazwa);
        $opis = $this->_db->escapeString($opis);
        
	$str = "UPDATE Galeria SET idDzialu = '$idDzialu', idGaleria = '$idGaleria', nazwa = '$nazwa', opis = '$opis' WHERE idGaleria='$idGaleria'";
	$sql = $this->_db->query($str);
    }

    public function usunGalerie($idGaleria)
    {
	$str = "DELETE FROM Galeria WHERE idGaleria = '$idGaleria' ";
        $sql = $this->_db->query($str);
    }

    public function pobierzGalerie($id)
    {
	$str = "SELECT *,`nazwaWyswietlana` FROM Galeria inner join `Uzytkownik` on (`idAutora` = `idUzytkownika`) WHERE idGaleria = '$idGaleria'";
        $this->_dane = $this->_db->query($str);
        
        if ( $this->_dane->count() )
        {
            return $this->_dane = $this->_dane->fetchAll ();
            $this->_dane = $this->_dane[0];
        } else
            $this->_dane = [];
        
        return !empty($this->_dane);
    }

    public function pobierzListeGalerii()
    {
	$str = "SELECT idGaleria, nazwa FROM Galeria";
        $this->_dane = $this->_db->query($str);
        
        if ( $this->_dane->count() )
        {
            $this->_dane = $this->_dane->fetchAll ();
            $this->_dane = $this->_dane[0];
        } else
            $this->_dane = [];
        
        return !empty($this->_dane);
        
    }
    
    public function pobierzListeGaleriizAutorem($dzial = null)
    {
     $str = "select `Galeria`.`idGalerii` as 'gal_id', `Galeria`.`nazwa` as 'gal_title', ".
            "`Galeria`.`opis` as 'gal_opis', `Uzytkownik`.`idUzytkownika` as 'user_id', ".
            "`Uzytkownik`.`nazwaWyswietlana` as 'user_name'".
            "from `Galeria` inner join `Uzytkownik` on (`Galeria`.`idAutora` = `Uzytkownik`.`idUzytkownika`) ".
            "where ";
            
     if ( $tylkoZaak )
      $str .= " and `Publikacja`.`akceptacja` = 1 ";
     
     if ( !is_null($dzial) )
      $str .= " and `Galeria`.`idDzialu` = {$dzial} ";
            
     $str .= " order by `Galeria`.`idGaleria` desc;";
     $str = $this->_db->query($str);
     
     return $str->fetchAll();
    }
}
