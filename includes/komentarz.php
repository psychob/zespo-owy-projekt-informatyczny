<?php

 class komentarz
 {
  static public function DodajKomentarz( $idPub, $idAutor, $tresc )
  {
   return self::_DodajKomentarz($idPub, $idAutor, $tresc);
  }
  
  static public function DodajKomentarzJakoGosc( $idPub, $nazwaAutora, $tresc )
  {
   return self::_DodajKomentarz($idPub, null, $tresc, $nazwaAutora);
  }
  
  static private function _DodajKomentarz( $idPub, $idAutor, $tresc, $nazwa = null )
  {
   global $db;
   
   // dodajemy do bazy danych
   $tresc = $db->escapeString($tresc);
   
   $sql = "insert into `Komentarz` (`idPublikacji`, `idAutora`, `dataDodania`, `tresc`, `typWpisu` ) values ( {$idPub}, ";
   
   if ( $idAutor == null )
   {
    // nie mamy autora
    $sql .= "null, ";
   } else
   {
    $sql .= "{$idAutor}, ";
   }
   
   $sql .= "NOW(), '".$tresc."', ";
   
   if ( $idAutor == null )
    $sql .= "'".$db->escapeString ($nazwa)."' ";
   else
    $sql .= 'null ';
   
   $sql .= ');';
   
   return $db->query($sql)->getBool() == true;
  }
 }