<?php

 require_once __DIR__.'/prawa.php';

 class Uzykownik
 {
  /**
   * @var MySQL
   */
  private $_db = null;
  private $_dane = [];
  /**
   * @var Prawa
   */
  private $_right = null;
  
  public function __construct(MySQL $db)
  {
   $this->_db = $db;
   
   $this->_right = new Prawa(0, $db);
  }
  
  public function isLogged( )
  {
   if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id']))
   {
    // pobieramy dane
    $this->_dane = $this->_db->query("select * from `Uzytkownik` where `idUzytkownika` = ".$_SESSION['user_id']);
    
    if ( $this->_dane->count() )
    {
     $this->_dane = $this->_dane->fetchAll ();
     $this->_dane = $this->_dane[0];
     
     $this->_right = new Prawa($this->_dane['idUzytkownika'], $this->_db);
    } else
     $this->_dane = [];
   }
   
   return !empty($this->_dane);
  }
  
  public function zaloguj( $login, $pass )
  {
   $login = $this->_db->escapeString($login);
   $pass  = $this->_db->escapeString($pass);
   
   $sql = "select * from `Uzytkownik` where `email` = '".$login."' and `haslo` = '".md5($pass)."' limit 1;";
   $sql = $this->_db->query($sql);
   
   if ( $sql->count() )
   {
    $_SESSION['user_id'] = $sql['idUzytkownika'];
    return 'ok';
   } else
    return 'nouser';
  }
  
  public function getUserInfo()
  {
   return $this->_dane;
  }
  
  public function wyloguj()
  {
   $_SESSION['user_id'] = null;
  }
  
  public function getRights( )
  {
   return $this->_right;
  }
  
  public function getID()
  {
   return $this->_dane['idUzytkownika'];
  }
 }