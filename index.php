<?php

// dołączamy wszystko co potrzebujemy
require_once './lib/smarty/Smarty.class.php';
require_once './lib/MySQL.php';

require_once './includes/Uzytkownik.php';

session_start();

$db = new MySQL("sql341389", "tI2!dP3%", "sql341389", "sql3.freesqldatabase.com");
$theme = new Smarty();
$user = new Uzykownik($db);

$theme->setCompileDir('./tpl/cmp');
$theme->setTemplateDir('./tpl/tpl');
$theme->setConfigDir('./tpl/cfg');

$theme->assign('user_logged', $user->isLogged());
$theme->assign('user_info', $user->getUserInfo());

$theme->debugging = true;

$what = isset($_GET['strona']) && !empty($_GET['strona']) ? $_GET['strona'] : 'index';

switch ($what) {
    case 'register':
        require_once './page-register.php';
        break;

    case 'login':
        require_once './page-login.php';
        break;

    case 'logout':
        require_once './page-logout.php';
        break;
    case 'publikacja':
        require_once './page-publikacja.php';
        break;
    case 'dzial':
        require_once './page-dzial.php';
        break;

    case 'index':
    default:
        require_once './page-index.php';
}

require_once './sidebar-dzial.php';

$theme->display('index.tpl');
