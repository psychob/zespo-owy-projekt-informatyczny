<?php
 $theme->assign('page', 'register');
 
 if ( isset($_GET['step']) && $_GET['step'] == '2' )
 {
  // walidacja danych
  if ( ( isset($_POST['login']) && !empty($_POST['login']) ) &&
       ( isset($_POST['haslo1']) && !empty($_POST['haslo1']) ) &&
       ( isset($_POST['haslo2']) && !empty($_POST['haslo2']) ) &&
       ( isset($_POST['email']) && !empty($_POST['email']) ) )
  {
   // wszystkie dane są
   
   // sprawdzamy czy poprawne
   if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false)
   {
    header('location: index.php?strona=register&error=email');
    die;
   }
   
   if ( $_POST['haslo1'] != $_POST['haslo2'] )
   {
    header('location: index.php?strona=register&error=pass');
    die;
   }
   
   // sprawdzamy czy jest już taki zainteresowany tutaj
   $login = $db->escapeString($_POST['email']);
   
   $sql = $db->query("select idUzytkownika from `Uzytkownik` where `email` = '".$login."';");
   if ( $sql->count() )
   {
    header('location: index.php?strona=register&error=already');
    die;
   }
   
   $sql = "insert into `Uzytkownik` (nazwaWyswietlana, email, haslo, idPrawa) ".
           "values ( '".$db->escapeString($_POST['login'])."', '".$login."', '".md5($_POST['haslo1'])."', 1);";
   
   $sql = $db->query($sql);
   
   header('location: index.php?strona=register&status=ok');
   die;
  } else
  {
   header('location: index.php?strona=register&error=empty');
   die;
  }
 }
 
 if ( isset($_GET['error']) && !empty($_GET['error']))
  $theme->assign('error', $_GET['error']);
 
 if ( isset($_GET['status']) && $_GET['status'] == 'ok' )
  $theme->assign('status', 'ok');